import fetch from 'isomorphic-unfetch'

// Method that returns a list of users from an API
export const usersFetching = async () => {
  const res = await fetch('https://dummyjson.com/users?select=id,firstName,lastName,email,username,birthDate,image,address');
  const data = await res.json();
  return data;
}

// Method that returns a single user from an API
export const usersFetchingParams = async (params: any) => {
  const res = await fetch(`https://dummyjson.com/users/${params.id}?select=id,firstName,lastName,email,username,birthDate,image,address`);
  const data = await res.json();
  return data;
}

// Method that returns a list of users from an API
export const usersFetchingPaths = async () => {
  const req = await fetch('https://dummyjson.com/users?select=id,firstName,lastName,email,username,birthDate,image,address');
  const data = await req.json();
  const users = data.users;
  return users;
}