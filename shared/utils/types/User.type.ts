export type User = {
  id: string,
  firstName: string,
  lastName: string,
  email: string,
  username: string,
  birthDate: string,
  image: string,
  address: {
    address: string,
    city: string,
    state: string
  }
}