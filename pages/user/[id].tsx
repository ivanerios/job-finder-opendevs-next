import { GetStaticPaths, GetStaticProps, NextPage } from 'next'
import { User } from '../../shared/utils/types/User.type'
import Image from 'next/image'
import Link from 'next/link'
import styles from '../../styles/User.module.css'
import { usersFetchingParams, usersFetchingPaths } from '../../shared/middlewares/dataFetching'
import { Header } from '../../components/header'
import { Footer } from '../../components/footer'

const UserDetail: NextPage = (props: any) => {

  // Data retrieved from getStaticProps
  const user: User = props.data

  return (
    <div>
    <div className={styles.container}>
    <div className={styles.main}>
      <Header />
    <div className={styles.card}>
          <Image className={styles.image} src={user.image} width='200' height='200' alt='User photo' />
          <div className={styles.info}><p><span className={styles.maintitle}>{user.firstName} {user.lastName}</span></p>
          <p><div className={styles.username}>@{user.username}</div></p>
          <p><span className={styles.subtitle}><strong>{user.address.address}</strong></span></p>
          <p><div className={styles.subtitle}>{user.address.city} - {user.address.state}</div></p>
          <p><Link href={`mailto:${user.email}`}><span className={styles.email}>{user.email}</span></Link></p>
          <p><span className={styles.subtitle}>{user.birthDate.slice(8)}/{user.birthDate.slice(5, -3)}/{user.birthDate.slice(0,-6)}</span></p></div>
          </div>
          <Link href='/userList'><div className={styles.cardalt}>&lt;</div></Link>
    </div>
    </div>
    <Footer /></div>
  )
}

export const getStaticProps: GetStaticProps = async ({ params }) => {

  // Call from a middleware to fetch data
  const data = await usersFetchingParams(params);
  return {
    props: { data },
  };
};

export const getStaticPaths: GetStaticPaths = async () => {

  // Call from a middleware to fetch data
  const users = await usersFetchingPaths();

  const paths = users.map((user: User) => {
    return { params: { id: user.id.toString() } }
  })

  return {
    paths,
    fallback: false
  }
};

export default UserDetail