import type { GetStaticProps, NextPage } from 'next'
import Image from 'next/image'
import Link from 'next/link'
import { User } from '../shared/utils/types/User.type'
import styles from '../styles/Home.module.css'
import { usersFetching } from '../shared/middlewares/dataFetching'
import { Header } from '../components/header'
import { Footer } from '../components/footer'
import { useEffect, useState } from 'react'
import search from '../public/icons8-búsqueda-24.svg'
import { useRouter } from 'next/router'

const Home: NextPage = (props: any) => {

  const router = useRouter();

  const searchQuery = () => {
    router.push(`/userList?search=${searchData}`)
  }

  const today = new Date();

  //Method that calculates the day of the year that is today
  const todayNumber: number = ((today.getMonth() + 1) * 30) + today.getDate();

  // Data retrieved from getStaticProps
  const users = props.data.users

  //Method that filter and order users forward from the present date to december 31st according to their birth date
  const usersFiltered = users
    .filter((user: User) => todayNumber < (parseInt(user.birthDate.slice(5, -3)) * 30) + parseInt(user.birthDate.slice(8)))
    .sort((a: User, b: User) => (parseInt(a.birthDate.slice(5, -3)) * 30) + parseInt(a.birthDate.slice(8)) < (parseInt(b.birthDate.slice(5, -3)) * 30) + parseInt(b.birthDate.slice(8)) ? -1 : 1)

  //Method that filter and order users forward from january 1st according to their birth date
  const usersRest = users
    .sort((a: User, b: User) => (parseInt(a.birthDate.slice(5, -3)) * 30) + parseInt(a.birthDate.slice(8)) < (parseInt(b.birthDate.slice(5, -3)) * 30) + parseInt(b.birthDate.slice(8)) ? -1 : 1)

  const usersSorted = usersFiltered.concat(usersRest)

  usersSorted.length = 10;

  const [searchData, setSearchData] = useState('')

  return (
    <div>
      <div className={styles.container}>
        <div className={styles.main}>
          <Header />
          <div className={styles.input}><input value={searchData} placeholder='Search hackers' onChange={e => setSearchData(e.target.value)} onKeyDown={e => e.key === 'Enter' && searchQuery()} /><div className={styles.button} onClick={searchQuery}><Image src={search} width='20' height='20' alt='Search button' /></div></div>
          <div className={styles.grid}>
            {usersSorted.map((user: User) => (
              <span key={user.id}><Link href={`/user/${user.id}`}><div className={styles.card}>
                <Image className={styles.image} src={user.image} width='70' height='70' alt='User photo' />
                <p><span className={styles.maintitle}>{user.firstName} {user.lastName}</span></p>
                <p><span className={styles.subtitle}>{user.address.city} - {user.address.state}</span></p>
                <p><Link href={`mailto:${user.email}`}><span className={styles.email}>{user.email}</span></Link></p>
                <p><span className={styles.subtitle}>{user.birthDate.slice(8)}/{user.birthDate.slice(5, -3)}/{user.birthDate.slice(0, -6)}</span></p>
              </div></Link></span> 
            ))}
          </div><Link href='/userList'><div className={styles.cardalt}>+</div></Link>
        </div>
      </div><Footer /></div>
  )
}

export const getStaticProps: GetStaticProps = async (context) => {

  // Call from a middleware to fetch data
  const data = await usersFetching();
  return {
    props: { data },
  };
};

export default Home
