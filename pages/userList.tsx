import Image from 'next/image'
import { GetStaticProps, NextPage } from "next"
import { useEffect, useState } from "react"
import { User } from '../shared/utils/types/User.type'
import Link from 'next/link'
import styles from '../styles/UserList.module.css'
import { usersFetching } from '../shared/middlewares/dataFetching'
import { Header } from '../components/header'
import { Footer } from '../components/footer'
import searcher from '../public/icons8-búsqueda-24.svg'
import { useRouter } from 'next/router'

const Userlist: NextPage = (props: any) => {

  const router = useRouter();

  const queryParam = router.query.search

  // Data retrieved from getStaticProps
  const users = props.data.users

  // Function to filter users according to input characters
  const search = () => {
    if (searchData === undefined || searchData === '') {
      router.replace('/userList')
      setUsersFiltered(users)
    }
    else {
      let dataChange = users.filter((user: User) => {
        return user.firstName.toLowerCase().includes(searchData) || user.firstName.toUpperCase().includes(searchData) || user.firstName.includes(searchData) ||
          user.lastName.toLowerCase().includes(searchData) || user.lastName.toUpperCase().includes(searchData) || user.lastName.includes(searchData) ||
          user.email.includes(searchData) ||
          user.address.city.toLowerCase().includes(searchData) || user.address.city.toUpperCase().includes(searchData) || user.address.city.includes(searchData) ||
          user.address.state.toLowerCase().includes(searchData) || user.address.state.toUpperCase().includes(searchData) || user.address.state.includes(searchData)
      })
      router.replace(`/userList?search=${searchData}`)
      setUsersFiltered(dataChange)
    }
  }

  // List of users that updates with every tipping change in the input field
  const [usersFiltered, setUsersFiltered] = useState(users)

  // Initial value for the input field, that updates with every tipping change
  const [searchData, setSearchData] = useState<any>(queryParam)

  // Method that allows to make the search automatic at the time you are tipping, it refreshes the search with every change
  useEffect(() => {
    search()
  }, [searchData])


  return (
    <div>
      <div className={styles.container}>
        <div className={styles.main}>
          <Header />
          <div className={styles.input}><input value={searchData} placeholder='Search hackers' onChange={e => setSearchData(e.target.value)} /><div className={styles.button} ><Image src={searcher} width='20' height='20' alt='Search button' /></div></div>
          <div className={styles.grid}>
            {usersFiltered.length > 0 ? usersFiltered.map((user: User) => (
              <span key={user.id}><Link href={`/user/${user.id}`}><div className={styles.card}>
                <Image className={styles.image} src={user.image} width='70' height='70' alt='User photo' />
                <p><span className={styles.maintitle}>{user.firstName} {user.lastName}</span></p>
                <p><span className={styles.subtitle}>{user.address.city} - {user.address.state}</span></p>
                <p><Link href={`mailto:${user.email}`}><span className={styles.email}>{user.email}</span></Link></p>
                <p><span className={styles.subtitle}>{user.birthDate.slice(8)}/{user.birthDate.slice(5, -3)}/{user.birthDate.slice(0, -6)}</span></p>
              </div></Link></span>
            )) : <span>Nothing found with this data, make a new search</span>}
          </div>
          <Link href='/'><div className={styles.cardalt}>&lt;</div></Link></div>
      </div><Footer /></div>
  )
}

export const getStaticProps: GetStaticProps = async (context) => {

  // Call from a middleware to fetch data
  const data = await usersFetching();
  return {
    props: { data },
  };
};

export default Userlist;