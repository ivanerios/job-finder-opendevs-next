import Image from 'next/image'
import logo from '../public/logo.svg'
import email from '../public/icons8-mail.svg'
import gitlab from '../public/gitlab-logo-600.svg'
import instagram from '../public/icons8-instagram.svg'
import styles from '../styles/Footer.module.css'
import Link from 'next/link'
import { useRouter } from 'next/router'

export const Footer = () => {

  const router = useRouter();

  const linkToHome = () => {
    router.push('/')
  }

  return (
    <div className={styles.footer}><div  className={styles.logo} onClick={linkToHome}><Image src={logo} width='25' alt='App Logo' />
    &nbsp; SEARCH-A-HACKER</div> 
    <div className={styles.text}>&copy; &nbsp; Ivan E. Ríos Huete &nbsp;
    <span className={styles.link}><Link href={'mailto:ivanerioshuete@gmail.com'}><Image src={email} width='25' height='25' alt='Link to My e-mail' /></Link></span>
    <span className={styles.link}><Link href={'https://gitlab.com/ivanerios'}><Image src={gitlab} width='35' height='35' alt='Link to My GitLab' /></Link></span>
    <span className={styles.link}><Link href={'https://www.instagram.com/ivanerios'}><Image src={instagram} width='25'  height='25' alt='Link to My Instagram' /></Link></span></div></div>
  )
}