import Image from 'next/image'
import { useRouter } from 'next/router';
import logo from '../public/logo.svg'
import styles from '../styles/Header.module.css'

export const Header = () => {

  const router = useRouter();

  const linkToHome = () => {
    router.push('/')
  }

  return (
    <div className={styles.headerdiv} onClick={linkToHome}><Image src={logo} width='50' alt='App Logo' />
    <h1>SEARCH-A-HACKER</h1></div>
  )
}